package com.example.assignment7register

import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.assignment7register.databinding.FragmentRegisterBinding
import com.example.assignment7register.network.NetworkClient
import kotlinx.coroutines.launch


class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {




    override fun start() {
        binding.back.setOnClickListener {
            back()
        }
        binding.register.setOnClickListener {
            register()
        }
    }

    private fun back() {
        findNavController().navigate(R.id.action_registerFragment_to_homeFragment)

    }


    private fun register() {

        if (binding.emailET.text.toString() != "" && binding.passwordET.text.toString() != "" && binding.repeatPasswordET.text.toString() != "" && Patterns.EMAIL_ADDRESS.matcher(binding.emailET.text).matches()) {
            binding.progressbar.isVisible = true
            viewLifecycleOwner.lifecycleScope.launch {
                val email = binding.emailET.text.toString()
                val password = binding.passwordET.text.toString()
                val response = NetworkClient.apiClient.RegisterUser(email, password)
                Log.d("Token", "logIn:${response.body().toString()} ")
                if (response.body() != null) {
                    binding.progressbar.isVisible = false
                    setFragmentResult("requestKey", bundleOf("bundleKey" to email))
                    setFragmentResult("requestKey1", bundleOf("bundleKey1" to password))
                    findNavController().navigate(R.id.action_registerFragment_to_homeFragment)
                }
            }
        } else {
            Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
        }


    }
}





