package com.example.assignment7register.data

data class Token(val token: String, val id: Int? = null)
