package com.example.assignment7register

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.assignment7register.databinding.FragmentHomeBinding
import com.example.assignment7register.network.NetworkClient
import kotlinx.coroutines.launch


class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener("requestKey") { requestKey, bundle ->
            val resultemail = bundle.getString("bundleKey")
            binding.emailET.setText(resultemail)
        }
        setFragmentResultListener("requestKey1") { requestKey1, bundle ->
            val resultpassword = bundle.getString("bundleKey1")
            binding.PasswordET.setText(resultpassword)
        }
    }

    override fun start() {
        binding.logIn.setOnClickListener {
            logIn()
        }
        binding.Register.setOnClickListener {
            registerButtonActivate()
        }

    }

    private fun logIn() {

        if (binding.emailET.text.toString() != "" && binding.PasswordET.text.toString() != "" && Patterns.EMAIL_ADDRESS.matcher(binding.emailET.text).matches()) {
            binding.progressbar.isVisible = true
            viewLifecycleOwner.lifecycleScope.launch {
                val email = binding.emailET.text.toString()
                val password = binding.PasswordET.text.toString()
                val response = NetworkClient.apiClient.logInUser(email, password)
                Log.d("Token", "logIn:${response.body().toString()} ")
                if (response.body() != null) {
                    binding.progressbar.isVisible = false
                    setFragmentResult("requestKey2", bundleOf("bundleKey2" to email))
                    findNavController().navigate(R.id.action_homeFragment_to_authorizedFragment)
                }
            }
        } else {
            Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
        }


    }

    private fun registerButtonActivate() {
        findNavController().navigate(R.id.action_homeFragment_to_registerFragment)
    }


}