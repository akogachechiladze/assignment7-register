package com.example.assignment7register

import android.os.Bundle
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.assignment7register.databinding.FragmentAuthorizedBinding


class AuthorizedFragment : BaseFragment<FragmentAuthorizedBinding>(FragmentAuthorizedBinding::inflate) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener("requestKey2") { requestKey2, bundle ->
            val resultemail = bundle.getString("bundleKey2")
            binding.emailTV.setText(resultemail)
        }
    }
    override fun start() {
        logOut()
    }

    private fun logOut() {
        binding.logOut.setOnClickListener {
            findNavController().navigate(R.id.action_authorizedFragment_to_homeFragment)
        }
    }

}