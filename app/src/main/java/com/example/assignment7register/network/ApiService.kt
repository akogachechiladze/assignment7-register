package com.example.assignment7register.network


import com.example.assignment7register.data.Token
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {
    @POST("api/login",)
    @FormUrlEncoded
    suspend fun logInUser(@Field("email") mail:String, @Field("password") password: String): Response<Token>


    @POST("api/register",)
    @FormUrlEncoded
    suspend fun RegisterUser(@Field("email") mail:String, @Field("password") password: String): Response<Token>

}